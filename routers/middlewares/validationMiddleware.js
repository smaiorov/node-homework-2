const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().alphanum().min(3).max(30).required(),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await schema
      .validateAsync(req.body)
      .then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};
