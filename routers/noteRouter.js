const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('./middlewares/authMiddleware');
const {Note} = require('../models/noteModel');

router.post('/', authMiddleware, async (req, res) => {
  const note = new Note({
    userId: req.user._id,
    text: req.body.text,
  });
  try {
    await note.save();
    res.status(200).json({message: 'Note created successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.get('/', authMiddleware, async (req, res) => {
  try {
    const {skip = 0, limit = 5} = req.query;

    const notes = await Note.find(
        {},
        ['userId', 'text', 'createdDate', 'completed'],
        {
          skip: parseInt(skip),
          limit: limit > 100 ? 5 : parseInt(limit),
          sort: {
            createdDate: -1,
          },
        },
    );
    res.status(200).json({notes: notes});
  } catch (err) {
    res.status(400).json({message: 'No notes found'});
  }
});

router.get('/:id', authMiddleware, async (req, res) => {
  const _id = req.params.id;

  const note = await Note.findOne({_id, userId: req.user._id}, {__v: 0});

  if (!note) {
    return res.status(400).json({message: 'Wrong note id'});
  }
  res.status(200).send(note);
});

router.put('/:id', authMiddleware, async (req, res) => {
  const _id = req.params.id;

  const note = await Note.findOne({_id, userId: req.user._id}, {__v: 0});
  if (!note) {
    return res.status(400).json({message: 'Wrong note id'});
  }

  if (note.text === req.body.text) {
    return res.status(400).json({message: 'Please edit your note'});
  }

  note.text = req.body.text;
  await note.save();
  res.status(200).json({message: 'Note edited successfully'});
});

router.patch('/:id', authMiddleware, async (req, res) => {
  const _id = req.params.id;

  const note = await Note.findOne({_id, userId: req.user._id}, {__v: 0});
  if (!note) {
    return res.status(400).json({message: 'Wrong note id'});
  }

  note.completed = !note.completed;
  await note.save();
  res.status(200).json({message: 'Success'});
});

router.delete('/:id', authMiddleware, async (req, res) => {
  const _id = req.params.id;
  const note = await Note.findOne({_id, userId: req.user._id}, {__v: 0});

  if (!note) {
    return res.status(400).json({message: 'Wrong note id'});
  }

  await Note.findOneAndDelete({
    _id,
    userId: req.user._id,
  });
  res.status(200).json({message: 'Success'});
});

module.exports = router;
