const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();
const {authMiddleware} = require('./middlewares/authMiddleware');
const {User} = require('../models/userModel');

router.get('/', authMiddleware, async (req, res) => {
  const user = await User.findById(req.user._id).exec();
  if (user) {
    return res.status(200).json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
  } else {
    return res.status(400).json({message: 'Please autheticate'});
  }
});

router.delete('/', authMiddleware, async (req, res) => {
  try {
    await User.findOneAndRemove(req.user._id, req.body);
    res.status(200).json({message: 'User removed successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.patch('/', authMiddleware, async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'Wrong password'});
  }
  if (newPassword) {
    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();
    return res.status(200).json({message: 'Password changed successfully'});
  }
});

module.exports = router;
