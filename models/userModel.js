const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
    {
      username: {
        type: String,
        require: true,
        uniq: true,
      },
      password: {
        type: String,
        required: true,
      },
      createdDate: {
        type: Date,
        default: Date.now(),
      },
      tokens: [
        {
          token: {
            type: String,
          },
        },
      ],
    },
    {
      collection: 'users',
    },
);

module.exports.User = mongoose.model('User', userSchema);
